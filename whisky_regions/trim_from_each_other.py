"""Module holding mutual region trimming code."""
from typing import Any, List

import geopandas as gpd


class TrimFromEachOther:
    """Trimmer class: clip regions from each other."""

    _regions: gpd.GeoDataFrame
    _trimmed_polygons: List[Any]
    
    def __init__(
            self,
            regions: gpd.GeoDataFrame,
    ) -> None:
        self._regions = regions
        
    def trim(self) -> gpd.GeoDataFrame:
        """Trim layers from each other using "Most Distilleries" method."""
        self._trimmed_polygons = []
        
        self._trim_polygons()
        self._reset_regions_geometry()
        
        return self._regions
    
    def _trim_polygons(self) -> None:        
        for index, self._region in self._regions.iterrows():
            self._less_populated_polygon = self._region["Polygon"]

            self._amalgamate_more_populated_polygons()
            self._trim_more_populated_amalagamated_polygon_from_smaller()
    
    def _amalgamate_more_populated_polygons(self) -> None:
        self._amalgamated_more_populated_polygon = self._regions[
                self._regions["Distilleries"] > self._region["Distilleries"]
            ]["Polygon"].unary_union
        
    def _trim_more_populated_amalagamated_polygon_from_smaller(self) -> None:
        if self._amalgamated_more_populated_polygon is not None:
            self._trimmed_polygons.append(
                self._less_populated_polygon.difference(
                    self._amalgamated_more_populated_polygon
                )
            )
        else:
            self._trimmed_polygons.append(self._less_populated_polygon)

    def _reset_regions_geometry(self) -> None:
        self._regions.set_geometry(
            col=self._trimmed_polygons,
            inplace=True,
        )