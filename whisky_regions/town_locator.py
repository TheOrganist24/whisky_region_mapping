"""Town location finder module."""
import os
import requests
from typing import Any, Dict, List

import numpy as np
from shapely.geometry import Point


class TownLocator:
    """Tool for finding town locations.
    
    Methods
    -------
    get_point(town) Point   Retrieves longitude and latitude for a given town
    """
    
    _town: str
    _owm_url: str
    _owm_key: str
    _retrieved_towns: List[Dict[str, Any]]
    _town_location: Dict[str, Any]
    _point: Point
    possible_locations: List[Point]
    
    def __init__(self) -> None:
        """Initialise and check environment."""
        self._set_openweathermap_api_key()
        
    def _set_openweathermap_api_key(self) -> None:
        if "OPEN_WEATHERMAP_API_KEY" in os.environ:
            self._owm_key = os.environ["OPEN_WEATHERMAP_API_KEY"]
        else:
            raise RuntimeError("'OPEN_WEATHERMAP_API_KEY' not set.")
    
    def get_point(self, town: str) -> Point:
        self._town = town
        
        self._set_up_url()
        self._retrieve_town_geodata()
        self._remove_non_scottish_towns()
        
        if not self._retrieved_towns:
            return np.nan
        
        self._extract_all_possible_towns()
        self._assume_first_town_is_the_correct_one()
        self._extract_point_data()

        return self._point
        
    def _set_up_url(self) -> None:
        self._owm_url = f"http://api.openweathermap.org/geo/1.0/direct?q={self._town}&limit=10&appid={self._owm_key}"
    
    def _retrieve_town_geodata(self) -> None:
        response: requests.Response = requests.get(self._owm_url)
        self._retrieved_towns = response.json()
    
    def _remove_non_scottish_towns(self) -> None:
        self._retrieved_towns = [
            locale
            for locale in self._retrieved_towns
            if ("state" in locale and locale["state"] == "Scotland")
        ]
    
    def _extract_all_possible_towns(self) -> None:
        self.possible_locations = [
            Point(town["lon"], town["lat"])
            for town
            in self._retrieved_towns
        ]
    
    def _assume_first_town_is_the_correct_one(self) -> None:
        self._town_location = self._retrieved_towns[0]
    
    def _extract_point_data(self) -> None:
        self._point = Point(self._town_location["lon"], self._town_location["lat"])
            