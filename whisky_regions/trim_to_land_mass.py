"""Module holding Land Mass trimming code."""
import geopandas as gpd


class TrimToLandMass:
    """Trimmer class: clip regions to land mass underneath."""
    
    _base: gpd.GeoDataFrame
    _layer: gpd.GeoDataFrame
    
    def __init__(
            self,
            base_land_mass: gpd.GeoDataFrame,
            layer: gpd.GeoDataFrame,
    ) -> None:
        self._base = base_land_mass
        self._layer = layer
        
    def trim(self) -> gpd.GeoDataFrame:
        """Trim layer to fit base layer."""
        self._layer.set_geometry(
            col=self._layer.intersection(
                other=self._base.loc[1,"geometry"]
            ),
            inplace=True,
        )

        return self._layer