"""Module holding Land Mass trimming code."""
from IPython.display import clear_output, display
import geopandas as gpd
from shapely.ops import unary_union


class ExpandToFillLandMass:
    """Expansion class: expand to fill land mass underneath."""
    
    _base: gpd.GeoDataFrame
    _layer: gpd.GeoDataFrame
    
    def __init__(
            self,
            base_land_mass: gpd.GeoDataFrame,
            layer: gpd.GeoDataFrame,
    ) -> None:
        self._base = base_land_mass
        self._layer = layer
        
    def expand(self) -> gpd.GeoDataFrame:
        """Expand layer to fit base layer."""
        self._create_empty_polygons_from_land_mass_not_covered()
        self._add_region_centres_to_layer()
        self._merge_each_empty_zone_with_appropriate_region()

        return self._layer
    
    def _create_empty_polygons_from_land_mass_not_covered(self) -> None:
        empty_polygons = self._base.loc[1,"geometry"].difference(
            other=self._layer.unary_union,
        )
        self._empty_zones = gpd.GeoSeries([empty_polygons]).explode(
            index_parts=True,
        )
        
    def _add_region_centres_to_layer(self) -> None:
        self._layer["Centroid"] = self._layer.centroid

    def _merge_each_empty_zone_with_appropriate_region(self) -> None:
        self._setup_loop_counters()

        for zone_index, self._zone in self._empty_zones.items():
            self._winning_region_index = None
            self._winning_distance = None
            self._counter += 1

            self._find_appropriate_region_for_merging()
            self._merge_zones_into_regions()
            
            self._display_progress()
    
    def _setup_loop_counters(self) -> None:
        self._counter_total = len(self._empty_zones)
        self._counter = 0
        
    def _find_appropriate_region_for_merging(self) -> None:
        for self._region_index, self._region in self._layer.iterrows():
            self._run_tournament()
        
    def _run_tournament(self) -> None:
        if self._winning_region_index is None:
            self._first_challenger_as_winner_as_it_is_the_only_contester()
        else:
            self._setup_challenger()
            self._compare_distances()
                
    def _first_challenger_as_winner_as_it_is_the_only_contester(self) -> None:
        self._winning_region_index = self._region_index
        self._winning_distance = self._region["Centroid"].distance(
            self._zone.centroid
        )

    def _setup_challenger(self) -> None:
        self._challenger_distance = self._region["Centroid"].distance(
            self._zone.centroid
        )

    def _compare_distances(self) -> None:
        if self._challenger_distance < self._winning_distance:
            self._winning_region_index = self._region_index
            self._winning_distance = self._challenger_distance

    def _merge_zones_into_regions(self) -> None:
        self._layer.at[self._winning_region_index, "Polygon"] = unary_union(
            [
                self._layer["Polygon"][self._winning_region_index],
                self._zone,
            ]
        )

    def _display_progress(self) -> None:
        clear_output(wait=True)
        print(f"{self._counter}/{self._counter_total}")