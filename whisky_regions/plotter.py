"""Whisky Plotter module."""
import geopandas as gpd
import matplotlib
import matplotlib.pyplot as plt


class Plotter:
    """Class for plotting whisky distribution maps."""
    
    _base_gpd: gpd.GeoDataFrame
    
    def __init__(self, base_map: gpd.GeoDataFrame) -> None:
        """Initialise with bas plot."""
        self._base_gpd = base_map
        
        self._create_base_plot()
    
    def _create_base_plot(self) -> None:
        self.plot = self._base_gpd.to_crs(epsg=4326).plot(
            figsize=(8,13),
            color="white",
            edgecolor="black",
        )
    
    def add_distilleries(self, distilleries: gpd.GeoDataFrame) -> None:
        self.distilleries = distilleries.to_crs(epsg=4326).plot(
            ax=self.plot,
            marker="x",
            color="red",
        )
    
    def add_regions(self, regions: gpd.GeoDataFrame) -> None:
        self.regions = regions.to_crs(epsg=4326).plot(
            ax=self.plot,
            column="Name",
            categorical=True,
            #alpha=0.5,
            legend=True,
            legend_kwds={
                "loc": "upper left",
            },
        )

    def save_figure(self, suffix: str) -> None:
        self.plot.get_figure().savefig(f"output/whisky_regions_{suffix}.png")