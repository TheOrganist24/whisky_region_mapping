# Whisky Regions

## To run
```bash
poetry install
export OPEN_WEATHERMAP_API_KEY=<key obtained from OpenWeatherMap>
poetry run jupyter lab
```

## Exported Shapefiles
These should be able to be used directly once generate.

To archive them use `cd output && tar cvf ../whisky_regions.tar.gz whisky_regions.*`.

## Attribution
* Distillery list from Wikipedia
* Town location data from openweathermap.org
